let slider = document.querySelector("#userSlider");
let value = document.querySelector("#value");
let card1 = document.querySelector("#card1");
let card2 = document.querySelector("#card2");
let card3 = document.querySelector("#card3");
value.textContent = slider.value;
slider.oninput = function () {
  value.textContent = this.value;
  if (this.value >= 0 && this.value <= 10) {
    card1.classList.add("highlight");
    card2.classList.remove("highlight");
    card3.classList.remove("highlight");
  }
  if (this.value >= 11 && this.value <= 20) {
    card2.classList.add("highlight");
    card1.classList.remove("highlight");
    card3.classList.remove("highlight");
  }
  if (this.value >= 21) {
    card3.classList.add("highlight");
    card1.classList.remove("highlight");
    card2.classList.remove("highlight");
  }
};

document
  .querySelectorAll('[data-bs-target="#exampleModal"]')
  .forEach((button) => {
    button.addEventListener("click", function () {
      const title = this.getAttribute("data-title");
      document.querySelector(".modal-title").textContent = `${title}`;
    });
  });

let name = document.querySelector("#firstname");
let email = document.querySelector("#email");

let continueBtn = document.getElementById("continue");

continueBtn.addEventListener("click", function () {
  const nameLabel = document.querySelector('label[for="firstname"]');
  const mailLabel = document.querySelector('label[for="email"]');

  nameLabel.style.color = "black";
  mailLabel.style.color = "black";

  if (name.value.trim() === "") {
    nameLabel.style.color = "red";
  }

  if (email.value.trim() === "") {
    mailLabel.style.color = "red";
  }

  if (name.value.trim() !== "" && email.value.trim() !== "") {
    document.querySelector(".modal").classList.remove("show");
    document.querySelector(".modal-backdrop").remove();
  }
});

const resultsContainer = document.getElementById("results");

let page = 1;
let loading = false;

function fetchData() {
  if (loading) return;
  loading = true;

  fetch(`https://randomuser.me/api/?results=10&page=${page}`)
    .then((response) => response.json())
    .then((data) => {
      data.results.forEach((user) => {
        const userElement = document.createElement("div");
        userElement.classList.add("user");
        userElement.innerHTML = `
                    <img src="${user.picture.medium}" alt="${user.name.first}">
                    <h2>${user.name.first} ${user.name.last}</h2>
                    <p>Email: ${user.email}</p>
                `;
        resultsContainer.appendChild(userElement);
      });

      page++;
      loading = false;
    })
    .catch((error) => {
      console.error("Error fetching data:", error);
      loading = false;
    });
}

window.addEventListener("scroll", () => {
  if (window.innerHeight + window.scrollY >= document.body.offsetHeight - 100) {
    fetchData();
  }
});
